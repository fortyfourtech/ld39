﻿using System;


public enum MoveDirection {
	left,
	right,
	nowhere
}

public enum Track {
	left,
	right,
	center
}

public enum BackgroundTrack {
	left,
	right
}
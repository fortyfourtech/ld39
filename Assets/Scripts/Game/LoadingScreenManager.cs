﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScreenManager : MonoBehaviour {
	[SerializeField] private GameObject[] poolPrefabs;
	private List<GameObjectsPool> m_pools = new List<GameObjectsPool>();

	[SerializeField] private LoadingBottle m_bottle;
	private Animator m_bottleAnimator;

	bool roadTilesLoaded = false;

	void Start() {
		m_bottleAnimator = m_bottle.GetComponent<Animator> ();

		spawnPools ();

		foreach (var pool in m_pools) {
			pool.poolFilledEvent += checkPoolFilled;
		}

		m_bottle.flipFinished += finishLoading;

		checkPoolFilled ();
	}

	void spawnPools () {
		foreach (var prefab in poolPrefabs) {
			var pool = GameObject.Find ("/" + prefab.name);
			var poolClone =  GameObject.Find ("/" + prefab.name + "(Clone)");
			if (pool == null && poolClone == null) {
				pool = Instantiate (prefab) as GameObject;
			} else if (pool == null)
				pool = poolClone;

			var poolComponent = pool.GetComponent<GameObjectsPool> ();
			m_pools.Add (poolComponent);
		}
	}

	void checkPoolFilled () {

		var allFilled = true;
		foreach (var pool in m_pools) {
			if (!pool.isFilled) {
				allFilled = false;
				break;
			}
		}

		if (allFilled) {
			startFinishingSequence ();
		}
	}

	void startFinishingSequence() {
		m_bottleAnimator.SetTrigger ("end");
	}

	void finishLoading() {
		SceneManager.LoadScene (1);
	}
}

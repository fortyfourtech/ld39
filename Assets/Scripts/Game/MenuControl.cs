using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class MenuControl : MonoBehaviour
{
	private bool m_JumpAction = false;
    
    private void Update()
    {
		if (!m_JumpAction)
			m_JumpAction = CrossPlatformInputManager.GetButtonDown("Jump");
    }


    private void FixedUpdate()
    {
        if (m_JumpAction)
            Application.LoadLevel(1);
		m_JumpAction = false;
    }
}

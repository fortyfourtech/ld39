﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBottle : MonoBehaviour {
	public delegate void BottleEvent();
	public event BottleEvent flipFinished;

	void finishFlip() {
		if (flipFinished != null)
			flipFinished ();
	}
}

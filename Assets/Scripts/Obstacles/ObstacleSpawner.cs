﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleSpawner : MonoBehaviour {
	public float spawnRate = 1;
	public float spawnDistance = 15;
	public Transform leftTrack;
	public Transform centerTrack;
	public Transform rightTrack;

	public WorldParameters world;

//	[SerializeField] private Transform obstaclePool;
	private GameObjectsPool m_obstaclePool;

    private float m_spawnDelta = 2;
	private float m_lastSpawnDelta = 0;

	void Start() {
		m_obstaclePool = GameObject.Find("/ObstaclePool(Clone)").GetComponent<GameObjectsPool> ();
	}

	void Update(){
		m_lastSpawnDelta += Time.deltaTime;
		if (m_lastSpawnDelta >= m_spawnDelta / spawnRate) {
			m_lastSpawnDelta = 0;
			SpawnObstacle();
		}
	}

	void SpawnObstacle() {
		float randomSeed = Random.value*100;
		Track spawnInTrack = Track.center;
		if (randomSeed > 66)
			spawnInTrack = Track.left;
		else if (randomSeed > 33)
			spawnInTrack = Track.right;

		var spawned = m_obstaclePool.queueRandomTile ();

		spawned.GetComponent<WorldObject> ().worldParameters = world;

        Transform track2spawn;
		switch (spawnInTrack) {
		case Track.left:
		    track2spawn = leftTrack;
			break;
		case Track.right:
		    track2spawn = rightTrack;
			break;
		default:
			track2spawn = centerTrack;
			break;
		}

		spawned.position = track2spawn.position;
	}

	void OnDestroy() {
		m_obstaclePool.returnAll ();
	}
}

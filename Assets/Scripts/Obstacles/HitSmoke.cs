﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class HitSmoke : MonoBehaviour {
	ParticleSystem m_particles;

	void Awake() {
		m_particles = GetComponent<ParticleSystem> ();
	}

	void Update() {
		if (!m_particles.isEmitting)
			Destroy (gameObject);
	}
}

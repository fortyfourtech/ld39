using System;
using System.Collections;
using UnityEngine;

public class RunningCharacter : MonoBehaviour
{
	public Transform leftTrack;
	public Transform centerTrack;
	public Transform rightTrack;
    private bool m_Grounded = true;            // Whether or not the player is grounded.
    private Animator m_Anim;            // Reference to the player's animator component.
	private Track m_currentTrack;

	private float k_moveTimeDuration = 0.2f;
	private IEnumerator m_moveCoroutine;

    public delegate void TriggerEvent();
    public event TriggerEvent movePlayer;
    public event TriggerEvent increaseEnergy;
	public event TriggerEvent decreaseEnergy;
	public event TriggerEvent increasePower;

	AudioSource vapeSound;
	AudioSource spinnnerSound;
	AudioSource bonusSound;
	AudioSource hitSound;

	private bool dead = false;

    private void Awake()
    {
        m_Anim = GetComponent<Animator>();
		m_currentTrack = Track.center;

		vapeSound = transform.Find("sounds/vape").GetComponent<AudioSource> ();
		spinnnerSound = transform.Find("sounds/spinner").GetComponent<AudioSource> ();
		bonusSound = transform.Find("sounds/congrats").GetComponent<AudioSource> ();
		hitSound = transform.Find("sounds/hit").GetComponent<AudioSource> ();
    }

	public void Move(MoveDirection move, String action)
    {
		if (m_Grounded && ((action == "left")||(action == "right")) && !dead) 
        {
			if (move != MoveDirection.nowhere) {
				var trackChanged = false;

				switch (m_currentTrack) {
				case Track.center:
					m_currentTrack = move == MoveDirection.left ? Track.left : Track.right;
                            trackChanged = true;
					break;
				case Track.left:
					if (move == MoveDirection.right) {
						m_currentTrack = Track.center;
                            trackChanged = true;
					}
					break;
				case Track.right:
					if (move == MoveDirection.left) {
						m_currentTrack = Track.center;
						trackChanged = true;
					}
					break;
				default:
					m_currentTrack = Track.center;
					break;
				}
                if (action == "left") startAnimLeft();
                if (action == "right") startAnimRight();

                if (trackChanged) {
					// start move coroutine
					if (m_moveCoroutine != null) {
						StopCoroutine (m_moveCoroutine);
					}
					m_moveCoroutine = changeTrack ();
					StartCoroutine (m_moveCoroutine);

					// fire move event
					if (movePlayer != null) movePlayer();
				}
			}
        }

        if (m_Grounded && (action == "jump") && !dead)
        {
            if (movePlayer != null) movePlayer(); //evrnt movePlayer for -power
            StartAnimJump();
        }

        if (m_Grounded && (action == "increaseEnergy")) addEnergy();
		if (m_Grounded && (action == "increasePower")) addPower();
    }

    void startAnimLeft()
    {
		m_Anim.SetInteger("turn", -1);
    }

    void stopAnimLeft()
    {
		m_Anim.SetInteger("turn", 0);
    }

    void startAnimRight()
    {
		m_Anim.SetInteger("turn", 1);
    }

    void stopAnimRight()
    {
		m_Anim.SetInteger("turn", 0);
    }

	void startVape() {
		if (!vapeSound.isPlaying)
			vapeSound.Play ();
		m_Anim.SetTrigger("Vape");
	}

	void startSpinner() {
		m_Anim.SetTrigger("Spinner");
		if (!spinnnerSound.isPlaying)
			spinnnerSound.Play ();
	}

    void StopAnimJump()
    {
        m_Grounded = true;
        m_Anim.SetBool("Grounded", m_Grounded);
    }

    void StartAnimJump()
    {
        m_Grounded = false;
        m_Anim.SetBool("Grounded", m_Grounded);
    }

    void stopAnimBonus()
    {
        m_Anim.SetBool("Bonus", false);
		bonusSound.Stop ();
    }

    public void startAnimBonus()
    {
        m_Anim.SetBool("Bonus", true);
		bonusSound.Play ();
    }

    public void takeDamage()
    {
//        startAnimSpinner();
        if (decreaseEnergy != null) decreaseEnergy();

		m_Anim.SetTrigger ("hit");
		hitSound.Play ();
    }

    public void addEnergy()
    {
        startVape();
        if (increaseEnergy != null) increaseEnergy();
	}

	public void addPower()
	{
		startSpinner();
		if (increasePower != null) increasePower();
	}

    IEnumerator changeTrack() {
		var moveStartTime = Time.time;
		var moveDeltaTime = 0f;
		var moveFinishTime = moveStartTime + k_moveTimeDuration;

		var moveStartX = transform.position.x;

		Vector3 trackPos;
		switch (m_currentTrack) {    
		case Track.center:
			trackPos = centerTrack.position;
			break;
		case Track.left:
			trackPos = leftTrack.position;
			break;
		case Track.right:
			trackPos = rightTrack.position;
			break;
		default:
			trackPos = centerTrack.position;
			break;
		}
		var moveFinishX = trackPos.x;

		while (moveDeltaTime <= k_moveTimeDuration) {
			var currentXPos = Mathf.Lerp (moveStartX, moveFinishX, moveDeltaTime / k_moveTimeDuration);
			transform.position = new Vector3 (currentXPos, transform.position.y, transform.position.z);
			moveDeltaTime = Time.time - moveStartTime;
			yield return new WaitForFixedUpdate();
		}

		// make sure that pawn is reached exactly finish point
		transform.position = new Vector3 (moveFinishX, transform.position.y, transform.position.z);

		// that is for indicating that coroutine is finished
		m_moveCoroutine = null;
	}

	public void die() {
		dead = true;
		m_Anim.SetTrigger ("dead");
	}
}

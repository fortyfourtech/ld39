﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {
	[SerializeField] private Shader shader;
	[SerializeField] private Texture2D jumpBtnTxt;
    [SerializeField] private Texture2D texture2show;
    [SerializeField] private PlayerState playerState;
    //[SerializeField]
    private Texture net;
    //[SerializeField]
    private Texture back;
    private Material _shaderMaterial;
    // Use this for initialization
    void Awake(){
        //GetComponent<Camera>().SetReplacementShader(shader, null);
        //GetComponent<Camera> ().targetTexture = texture2save;
        net = Resources.Load<Texture>("net2x_pattern");
        back = Resources.Load<Texture>("screen_background");
        _shaderMaterial = new Material(shader);
    }

    //void OnGUI()
    //{
    //}

    void Update()
    {
        //    GetComponent<Camera>().RenderWithShader(shader, null);
        _shaderMaterial.SetVector("_CellSize", new Vector4(1 / 90.0f, 1 / 160.0f));
        _shaderMaterial.SetTexture("_NetPattern", net);
        _shaderMaterial.SetTexture("_Background", back);
    }
    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        //Material mat = new Material(shader);
        if (jumpBtnTxt != null)
        {
            RenderTexture.active = src;
            GL.PushMatrix();                                //Saves both projection and modelview matrices to the matrix stack.
            GL.LoadPixelMatrix(0, 90, 160, 0);            //Setup a matrix for pixel-correct rendering.
                                                          //Draw my stampTexture on my RenderTexture positioned by posX and posY.
        GUI.Label(new Rect(40, 0, 50, 15), "123456");
            Graphics.DrawTexture(new Rect(90 - jumpBtnTxt.width, 160 - jumpBtnTxt.height, jumpBtnTxt.width - 7, jumpBtnTxt.height - 7), jumpBtnTxt); // some magic here. never ask about it
            GL.PopMatrix();
            RenderTexture.active = null;
        }
        if (texture2show != null)
        {
            RenderTexture.active = src;
            GL.PushMatrix();                                //Saves both projection and modelview matrices to the matrix stack.
            GL.LoadPixelMatrix(0, 90, 160, 0);            //Setup a matrix for pixel-correct rendering.
                                                          //Draw my stampTexture on my RenderTexture positioned by posX and posY.
            Graphics.DrawTexture(new Rect(0, 0, texture2show.width, texture2show.height), texture2show);
            GL.PopMatrix();
            RenderTexture.active = null;
        }
        //GUI.DrawTexture(new Rect(0, 0, texture2save.width, texture2save.height), texture2save);
        Graphics.Blit(src, dst, _shaderMaterial);
    }
}

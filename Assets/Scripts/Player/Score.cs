﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {
    [SerializeField]
    private float rateScore = 0.00000005f;
    private float raundScore = 500;
    private float currentRaundScore = 0;
    private float score = 0;

    public int bestScore;
    public int lastScore;

    void Awake () {
        bestScore = PlayerPrefs.GetInt("bestScore");
        lastScore = PlayerPrefs.GetInt("lastScore");
    }

    public void increase( WorldParameters worldParameters) {
        score += worldParameters.currentSpeedVector.magnitude * rateScore;
    }

    public bool isRound()
    {
        score++;
        if ((score >= currentRaundScore-10) && (score <= currentRaundScore + 10))
        {
            currentRaundScore += raundScore;
            return true;
        }
        return false;
    }

    public int getScore()
    {
        return (int) score;
    }

    public int getCurrentBest()
    {
        return (bestScore > (int)score) ? bestScore : (int)score;
    }

    public void saveResult()
    {
        PlayerPrefs.SetInt("bestScore", getCurrentBest());
        PlayerPrefs.SetInt("lastScore", (int) score);
    }
}

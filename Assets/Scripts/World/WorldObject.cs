﻿using UnityEngine;

public class WorldObject : MonoBehaviour {
    public WorldParameters worldParameters;

    void FixedUpdate() {
		transform.position = transform.position + worldParameters.currentSpeedVector;
    }
}

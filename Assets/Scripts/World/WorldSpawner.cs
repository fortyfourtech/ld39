﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldSpawner : MonoBehaviour {
//	public float spawnRate = 1;
//	public float spawnDistance = 15;
//	public Transform leftTrack;
//	public Transform centerTrack;
//	public Transform rightTrack;

	[SerializeField] private Transform terminator;

	[SerializeField] private float tileSize = 4;

//	[SerializeField] private Transform roadPool;
//	[SerializeField] private Transform groundPool;
//	[SerializeField] private Transform bgPool;
	private GameObjectsPool m_roadPool;
	private GameObjectsPool m_groundPool;
	private GameObjectsPool m_bgPool;
//    [SerializeField] private GameObject obstacleDownPrefab;
	public WorldParameters world;
	
	[SerializeField] private SpawnerTrigger rightTrackSpawn;
	[SerializeField] private SpawnerTrigger centerTrackSpawn;
	[SerializeField] private SpawnerTrigger leftTrackSpawn;
	
	[SerializeField] private SpawnerTrigger rightGroundSpawn;
	[SerializeField] private SpawnerTrigger leftGroundSpawn;
	
	[SerializeField] private SpawnerTrigger rightBackgroundSpawn;
	[SerializeField] private SpawnerTrigger leftBackgroundSpawn;

    private float m_bgSpawnDelta = 2;
	private float m_lastLeftBgSpawnDelta = 0;
	private float m_lastRightBgSpawnDelta = 0;

	void Start() {
		m_roadPool = GameObject.Find("/RoadTilesPool(Clone)").GetComponent<GameObjectsPool> ();
		m_groundPool = GameObject.Find("/GroundTilesPool(Clone)").GetComponent<GameObjectsPool> ();
		m_bgPool = GameObject.Find("/RoadsideTilesPool(Clone)").GetComponent<GameObjectsPool> ();

		rightTrackSpawn.empty += delegate() { SpawnTrack(Track.right); };
		centerTrackSpawn.empty += delegate() { SpawnTrack(Track.center); };
		leftTrackSpawn.empty += delegate() { SpawnTrack(Track.left); };
		
		rightGroundSpawn.empty += delegate() { SpawnGround(BackgroundTrack.right); };
		leftGroundSpawn.empty += delegate() { SpawnGround(BackgroundTrack.left); };

		rightBackgroundSpawn.empty += delegate() { SpawnBackground(BackgroundTrack.right); };
		leftBackgroundSpawn.empty += delegate() { SpawnBackground(BackgroundTrack.left); };

		placeStartingTiles();
	}

	void SpawnTrack(Track track) {
		SpawnerTrigger spawnPoint;
		switch (track) {
		case Track.right:
			spawnPoint = rightTrackSpawn;
			break;
		case Track.center:
			spawnPoint = centerTrackSpawn;
			break;
		case Track.left:
			spawnPoint = leftTrackSpawn;
			break;
		default:
			spawnPoint = centerTrackSpawn;
			break;
		}

		var tileToSpawn = m_roadPool.queueTile();

		var worldComponent = tileToSpawn.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		spawnPoint.SpawnNext (tileToSpawn);
	}

	void SpawnGround(BackgroundTrack track) {
		SpawnerTrigger spawnPoint = track == BackgroundTrack.right ?
			rightGroundSpawn :
			leftGroundSpawn;

		var tileToSpawn = m_groundPool.queueTile();

		var worldComponent = tileToSpawn.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		spawnPoint.SpawnNext (tileToSpawn);
	}

	void SpawnBackground(BackgroundTrack track) {
		SpawnerTrigger spawnPoint = track == BackgroundTrack.right ?
			rightBackgroundSpawn :
			leftBackgroundSpawn;
		
		var tileToSpawn = m_bgPool.queueRandomTile();

		var worldComponent = tileToSpawn.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		spawnPoint.SpawnNext (tileToSpawn);
	}

	void placeStartingTiles() {
		var startX = leftTrackSpawn.transform.position.x;
		var endX = rightTrackSpawn.transform.position.x;
		var startZ = terminator.position.z;
		var endZ = transform.position.z;
		var yPos = centerTrackSpawn.transform.position.y;

		// Place road and roadside		
		var sideTile = m_groundPool.queueTile ();

		var worldComponent = sideTile.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		leftGroundSpawn.SpawnTile (sideTile, startZ);
		while (leftGroundSpawn.isEmpty) {
			sideTile = m_groundPool.queueTile ();

			worldComponent = sideTile.GetComponent<WorldObject> ();
			worldComponent.worldParameters = world;

			leftGroundSpawn.SpawnNext (sideTile);
		}
		sideTile = m_groundPool.queueTile ();

		worldComponent = sideTile.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		rightGroundSpawn.SpawnTile (sideTile, startZ);
		while (rightGroundSpawn.isEmpty) {
			sideTile = m_groundPool.queueTile ();

			worldComponent = sideTile.GetComponent<WorldObject> ();
			worldComponent.worldParameters = world;

			rightGroundSpawn.SpawnNext (sideTile);
		}

		var roadTile = m_roadPool.queueTile ();

		worldComponent = roadTile.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		leftTrackSpawn.SpawnTile (roadTile, startZ);

		while (leftTrackSpawn.isEmpty) {
			roadTile = m_roadPool.queueTile ();

			worldComponent = roadTile.GetComponent<WorldObject> ();
			worldComponent.worldParameters = world;

			leftTrackSpawn.SpawnNext (roadTile);
		}

		roadTile = m_roadPool.queueTile ();

		worldComponent = roadTile.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		centerTrackSpawn.SpawnTile (roadTile, startZ);

		while (centerTrackSpawn.isEmpty) {
			roadTile = m_roadPool.queueTile ();

			worldComponent = roadTile.GetComponent<WorldObject> ();
			worldComponent.worldParameters = world;

			centerTrackSpawn.SpawnNext (roadTile);
		}

		roadTile = m_roadPool.queueTile ();

		worldComponent = roadTile.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		rightTrackSpawn.SpawnTile (roadTile, startZ);

		while (rightTrackSpawn.isEmpty) {
			roadTile = m_roadPool.queueTile ();

			worldComponent = roadTile.GetComponent<WorldObject> ();
			worldComponent.worldParameters = world;

			rightTrackSpawn.SpawnNext (roadTile);
		}

		// Place buildings
		var buildingTile = m_bgPool.queueRandomTile ();

		worldComponent = buildingTile.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		leftBackgroundSpawn.SpawnTile(buildingTile, startZ);

		buildingTile = m_bgPool.queueRandomTile ();

		worldComponent = buildingTile.GetComponent<WorldObject> ();
		worldComponent.worldParameters = world;

		rightBackgroundSpawn.SpawnTile (buildingTile, startZ);

		while (leftBackgroundSpawn.isEmpty) {
			buildingTile = m_bgPool.queueRandomTile ();

			worldComponent = buildingTile.GetComponent<WorldObject> ();
			worldComponent.worldParameters = world;

			leftBackgroundSpawn.SpawnNext (buildingTile);
		}

		while (rightBackgroundSpawn.isEmpty) {
			buildingTile = m_bgPool.queueRandomTile ();

			worldComponent = buildingTile.GetComponent<WorldObject> ();
			worldComponent.worldParameters = world;

			rightBackgroundSpawn.SpawnNext (buildingTile);
		}

		// Place obstacles
	}
//	void SpawnObstacle() {
//		float randomSeed = Random.value*100;
//		Track spawnInTrack = Track.center;
//		if (randomSeed > 66)
//			spawnInTrack = Track.left;
//		else if (randomSeed > 33)
//			spawnInTrack = Track.right;
//
//		int prefabIndex = Random.Range (0, 3);
//        var indexPrefab = new Dictionary<int, GameObject> {
//            { 0,obstacleDownPrefab },
//            { 1,obstacleFullPrefab },
//            { 2,obstacleTallPrefab }
//        };
//        var indexType = new Dictionary<int, SettingDependent> {
//            { 0,SettingDependent.obstacleDown },
//            { 1,SettingDependent.obstacleFull },
//            { 2,SettingDependent.obstacleUp }
//        };
//        SettingDependent prefabType = indexType[prefabIndex];
//
//        GameObject spawnedO = Instantiate<GameObject>(indexPrefab[prefabIndex]);
//        Transform track2spawn = centerTrack;
//		switch (spawnInTrack) {
//		case Track.left:
//            if (prefabIndex != 1)
//			    track2spawn = leftTrack;
//			break;
//		case Track.right:
//            if (prefabIndex != 1)
//			    track2spawn = rightTrack;
//			break;
////		case Track.center:
////			trackX = centerTrack.position.x;
////			break;
////		default:
////			trackX = 0;
////			break;
//		}
//		float trackX = track2spawn.position.x;
////		float trackY = track2spawn.position.y;
//        float obsY = centerTrack.position.y + spawnedO.GetComponentInChildren<Renderer>().bounds.size.y/2; // or Collider
//		spawnedO.transform.position = new Vector3(trackX,obsY+spawnDistance,centerTrack.position.z+spawnDistance);
//        //spawnedO.GetComponent<SpriteRenderer>().sprite = SpriteProvider.GetSprite(Setting.jurassic, prefabType); // TODO: replace with prefabType // TODO: replace setting parameter
//        spawnedO.GetComponentInChildren<MySprite>().sprite = SpriteProvider.GetSprite(Setting.jurassic, prefabType); // TODO: replace with prefabType // TODO: replace setting parameter
//	}

	void OnDestroy() {
		m_roadPool.returnAll ();
		m_groundPool.returnAll ();
		m_bgPool.returnAll ();
	}
}

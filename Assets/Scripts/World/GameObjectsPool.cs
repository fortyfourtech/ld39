﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameObjectsPool : MonoBehaviour {
	public delegate void PoolEvent ();
	public event PoolEvent poolFilledEvent;

	private bool m_filled = false;

	[SerializeField] protected int poolSize = 100;
	protected List<Transform> tiles = new List<Transform>();

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad(gameObject);
		StartCoroutine (spawnPool ());
	}

	Transform queueTileAtIndex(int idx) {
		Transform result;
		if (tiles.Count > 0) {
			result = tiles [idx];
			tiles.RemoveAt (idx);
		} else {
			poolSize++;
			result = spawnAdditional ().transform;
		}

		result.gameObject.SetActive (true);
		return result;
	}

	abstract protected IEnumerator spawnPool ();
	abstract protected GameObject spawnAdditional ();

	public Transform queueTile() {
		return queueTileAtIndex(0);
	}

	public Transform queueRandomTile() {
		return queueTileAtIndex(Random.Range (0, tiles.Count));
	}

	public void returnTile(Transform usedTile) {
		usedTile.gameObject.SetActive (false);
		if (!tiles.Contains (usedTile))
			tiles.Add (usedTile);
	}

	public bool isFilled {
		get {
			return m_filled;
		}
	}

	public void returnAll() {
		for (var i = 0; i < transform.childCount; i++) {
			var tile = transform.GetChild (i);
			returnTile (tile);
		}
	}

	protected GameObject spawnTile(GameObject tilePrefab) {
		var newTile = Instantiate (tilePrefab, transform);

		newTile.GetComponent<Poolable> ().poolManager = this;

		newTile.SetActive (false);

		return newTile;
	}

	protected void TriggerFilledEvent() {
		m_filled = true;

		if (poolFilledEvent != null)
			poolFilledEvent ();
	}
}

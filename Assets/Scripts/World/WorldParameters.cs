﻿using UnityEngine;

public class WorldParameters : MonoBehaviour {
    [SerializeField] public float maxSpeed = 0.2f;
	[SerializeField] public Vector3 currentSpeedVector = Vector3.zero;
    public float accelerationUp = 0.01f;
    public float accelerationDown = 0.01f;
    private float stoppingSpeed = 0;
    public float delta = 0;

    private bool slowdown = false;
	private bool accerate = true;

    public void actionShock() {
		currentSpeedVector = Vector3.zero;
	}

	void FixedUpdate () {
		if (accerate) {
			var speed = currentSpeedVector.magnitude;
			var acceleration = 0f;
			if (speed > stoppingSpeed && slowdown ||
			   speed > maxSpeed)
				acceleration = accelerationDown * -1;
			else if (speed < maxSpeed && !slowdown ||
			        speed < stoppingSpeed)
				acceleration = accelerationUp;

			if (acceleration != 0)
				currentSpeedVector += Vector3.back * acceleration;
		}
    }

    public void actionSlowdown() {
        slowdown = true;
    }

    public void actionSpeedUp() {
        slowdown = false;
    }

	public void stop() {
		accerate = false;
		currentSpeedVector = Vector3.zero;
	}
}

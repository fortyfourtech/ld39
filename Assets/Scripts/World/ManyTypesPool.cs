﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManyTypesPool : GameObjectsPool {
	public GameObject[] tilePrefabs;

	override protected IEnumerator spawnPool() {
		int poolFill = 0;
		int prefabIndex = 0;
		while (poolFill < poolSize) {
			if (prefabIndex >= tilePrefabs.Length)
				prefabIndex = 0;

			yield return null;

			var newTile = spawnTile (tilePrefabs [prefabIndex]);
			tiles.Add (newTile.transform);

			prefabIndex++;
			poolFill++;
		}

		TriggerFilledEvent ();
	}

	protected override GameObject spawnAdditional () {
		var randomIndex = Random.Range (0, tilePrefabs.Length);
		var choosedPrefab = tilePrefabs [randomIndex];

		return spawnTile (choosedPrefab);
	}
}

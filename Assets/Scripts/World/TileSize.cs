﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSize : MonoBehaviour {

	[HideInInspector] public Vector3 size;

	void Awake() {
		var bounds = transform.Find ("bounds");
		if (bounds == null)
			bounds = transform.Find ("rot/bounds");
		var collider = bounds.GetComponent<BoxCollider> ();
		size = collider.size;
//		size = GetComponent<BoxCollider> ().size; // temp
	}
}

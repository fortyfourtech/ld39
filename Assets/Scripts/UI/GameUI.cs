﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {
	[SerializeField] private Image healthBar;
	[SerializeField] private Image energyBar;
	[SerializeField] private PlayerState player;

	private Energy m_energy;
	private Power m_power;

	void Start() {
		m_energy = player.GetComponent<Energy> (); 	// TODO: this is bad solution
		m_power = player.GetComponent<Power> ();	// TODO: rewrite it
	}
	
	// Update is called once per frame
	void Update () {
		healthBar.fillAmount = m_energy.currentValue / m_energy.maxValue;
		energyBar.fillAmount = m_power.currentValue / m_power.maxValue;
	}
}
